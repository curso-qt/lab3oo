#include "produtoalimenticio.h"

#include "iimposto.h"

void ProdutoAlimenticio::devolver() const
{
    _imposto->calcular();
    cout << "Devolvendo produto alimenticio" << endl;
}

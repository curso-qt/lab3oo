#include <iostream>
#include <memory>

using namespace std;

#include "produtoeletronico.h"
#include "produtoalimenticio.h"
#include "ipi.h"
#include "icms.h"
#include "iimposto.h"
#include "iproduto.h"

//Todas essas classes foram implementadas seguindo o pattern Bridge

auto main() -> int
{
//    unique_ptr<IProduto> produto { std::make_unique<ProdutoEletronico>() };
    unique_ptr<IProduto> produto { std::make_unique<ProdutoAlimenticio>() };
    shared_ptr<IImposto> imposto { std::make_shared<ICMS>() };
//    shared_ptr<IImposto> imposto { std::make_shared<IPI>() };
//    IImposto *imposto = new IPI;
//    IProduto *produto = new ProdutoEletronico;
//    IImposto *imposto = new ICMS;
//    IProduto *produto = new ProdutoAlimenticio;

    produto->setImposto(imposto);
    produto->devolver();

    return 0;
}

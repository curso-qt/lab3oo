#ifndef IPI_H
#define IPI_H

#include "iimposto.h"

class IPI : public IImposto
{
public:

    IPI() = default;
    virtual ~IPI() = default;

    float calcular() const override;
};

#endif // IPI_H

#include "produtoeletronico.h"

#include "iimposto.h"

void ProdutoEletronico::devolver() const
{
    _imposto->calcular();
    cout << "Devolvendo produto eletronico" << endl;
}

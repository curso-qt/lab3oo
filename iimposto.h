#ifndef IIMPOSTO_H
#define IIMPOSTO_H

#include <iostream>

using namespace std;

class IImposto{

public:
    virtual float calcular()  const = 0;

};

#endif // IIMPOSTO_H
